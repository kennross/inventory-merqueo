<p align="center">
<img src="https://laravel.com/assets/img/components/logo-laravel.svg">
</p>

## Prueba Backend Merqueo

Construir un sistema web de inventario de productos que conste de productos simples y
productos compuestos por otros (productos agrupados). Los productos deben tener al
menos los siguientes atributos: Nombre, referencia, precio, costo, unidades actuales y
estado, estos productos deben ser insertados directamente en la base de datos de forma
manual.
El sistema debe mostrar una grilla con todos los atributos de los productos

El sistema debe tener una opción para importar un archivo CSV con un listado de
comandos, estos comandos deben afectar los atributos del producto, la estructura de cada
línea del comando es la siguiente:

## Probar funcionamiento
```
CREATE DATABSE merqueo;

git clone git@gitlab.com:kennross/inventory-merqueo.git
cd inventory-merqueo
composer install
php artisan migrate
php artisan db:seed

php artisan dusk
```

Para revisar en el navegador
```
php artisan serve
```