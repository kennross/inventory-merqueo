<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Mostrando la vista para visualizar los productos y modal para cargar CSV
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('products')
            ->whereNull('product_id')
            ->paginate(8);

        return view('home', [
            'products' => $products
        ]);
    }

    /**
     * Lógica para carga y manipulación de archivo
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function uploadCsv(Request $request)
    {
        $feedback_commands = collect();

        /* Primera parte: Cargar archivo */
        $path = public_path() . '/storage/csv/';

        $name = 'command_current.' . $request->csv->getClientOriginalExtension();
        $request->file('csv')->move($path, $name);


        /* Lectura del archivo CSV */
        $handle = fopen(public_path() . '/storage/csv/' . $name, "r");

        /* Recorrer el CSV */
        $count = 0;
        while ($csvLine = fgetcsv($handle, 0, ",")) {
            $csvLine = $this->clearTrim($csvLine);

            if ($this->rowValid($csvLine)) {
                $this->manageCommand($csvLine,$feedback_commands);
            }

            $count++;
        }
        fclose($handle);

        session()->flash('status', 'El archivo ha sido cargado correctamente');
        session()->flash('feedback_commands', $feedback_commands);
        return redirect(route('home'));

    }

    /**
     * Validar si no hay filas con campos vacíos (en el caso de la referencia y el comando), para evitar error en el query
     * @param $row
     * @return bool
     */
    private function rowValid($row)
    {
        return $row[0] !== '' && $row[1] !== '';
    }

    /**
     * Limpiar los datos de espacios
     * @param $row
     * @return mixed
     */
    private function clearTrim($row)
    {
        foreach ($row as $key => $r) {
            $row[$key] = trim($r);
        }

        return $row;
    }

    /*
     * El sistema debe tener una opción para importar un archivo CSV con un listado de
     * comandos, estos comandos deben afectar los atributos del producto, la estructura de cada
     * línea del comando es la siguiente:
     * Identificador del producto, Nombre del comando, Parámetro.
     * Identificador del producto: Identificador del producto en la base de datos.
     * Nombre del comando: El listado de opciones disponibles son:
     * - Agregar: Agregar unidades al inventario.
     * - Restar: Descontar unidades al inventario.
     * - Activar: Cambiar el estado del producto para indicar que está activo.
     * - Desactivar: Cambiar el estado del producto para indicar que está desactivado.
     *
     * Identificación de líneas:
     * Indice 0: Código de referencia
     * Indice 1: Acción
     * Indice 2: Comando (opcional)
     *
     * Los comandos deberían ser en inglés y minúscula, pero lo dejaré en español, tal como el ejemplo
     * Recomendar a Scrum Master @ToDo
     *
     * Recomendación @ToDo
     * Mandar un mensaje de retroaliemntación para los casos de activación o desactivación de los productos
     * Es decir, si un producto activo está haciendo activo nuevamente, mandar un mensaje de feedback al usuario
     * Tarea Pendiente, presentar a Scrum Master
     */
    private function manageCommand($line, &$feedback_commands)
    {
        //Log::info($line[0]);
        //Log::info(Product::all()->toArray());
        $product = Product::whereReference($line[0])->first();

        $fb = [
            'product' => $product->name . ' (ref: '. $product->reference .')',
        ];

        switch ($line[1]) {
            case 'Agregar':
                //Log::info('Agregar');
                // Código para agregar
                $product->current_units += $line[2];
                $fb['action'] = 'Se le agregó ' . $line[2] . ' unidades';
                break;
            case 'Restar':
                //Log::info('Restar');
                // Código para restar
                $product->current_units -= $line[2];
                $fb['action'] = 'Se le resto ' . $line[2] . ' unidades';
                break;
            case 'Activar':
                //Log::info('Activar');
                // Código para activar
                $product->state = 'active';
                $fb['action'] = 'Fue activado';
                break;

            case 'Desactivar':
                //Log::info('Desactivar');
                // Código para restar
                $product->state = 'inactive';
                $fb['action'] = 'Fue desactivado';
                break;
        }

        $feedback_commands->push($fb);
        $product->save();

        //Log::info($fb);

        //Log::info('~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ');
    }
}
