<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'reference',
        'price',
        'cost',
        'current_units',
        'state',
        'product_id'
    ];

    /*
     * Relación que permitirá obtener los productos hijos al producto raíz
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'product_id');
    }

    public function isComposed() {
        return $this->products->count() > 0;
    }

}
