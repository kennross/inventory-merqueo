<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /*
         * USUARIO ADMIN
         */

        factory(\App\User::class)->create([
            'name' => 'Kennit Ruz Romero',
            'email' => 'kennit@merqueo.com',
            'password' => 'merqueo-in'
        ]);

        $this->call(ProductsTableSeeder::class);
    }
}
