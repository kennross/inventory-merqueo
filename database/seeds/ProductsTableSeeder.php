<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Se crearán 12 productos Simples */

        factory(\App\Product::class, 12)->create([
            'product_id' => null
        ]);


        /* Se crearán 4 productos Compuestos de 3 productos */

        $compound_products = factory(\App\Product::class, 4)->create([
            'product_id' => null
        ]);

        foreach ($compound_products as $cp) {
            factory(\App\Product::class, 3)->create([
                'product_id' => $cp->id
            ]);
        }
    }
}
