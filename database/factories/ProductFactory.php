<?php

use Faker\Generator as Faker;

$factory->define(\App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->streetName,
        'reference' => $faker->ean8,
        'price' => $faker->randomFloat('2', '1000', '1000000'),
        'cost' => $faker->randomFloat('2', '100', '500000'),
        'current_units' => $faker->numberBetween(0, 100000),
        'state' => $faker->randomElement(['active', 'inactive'])
    ];
});
