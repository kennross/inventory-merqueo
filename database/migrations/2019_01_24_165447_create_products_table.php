<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 120);
            $table->string('reference', 80);
            $table->double('price', 20, 5);
            $table->double('cost', 20, 5);
            /*
             * Teniendo en cuenta que las unidades son enteras, no decimales
             * En caso de que el requerimiento cambie a decimales, cambiar éste valor por Double
             * @ToDo, definir con el Scrum Master
             */
            $table->mediumInteger('current_units');
            /*
             * El requerimiento no especifica qué tipo de estados tendrá, se intuye que serán 2
             * En caso de que el requerimiento cambie a varios estados se debe crear una relación con una tabla de estados
             * @ToDo, definir con el Scrum Master
             */
            $table->enum('state', ['active', 'inactive'])->default('active');

            /*
             * Relación reflexiba (ejemplo: categorías de categorías)
             * Se permite el NULL el el caso de que sea un producto SIMPLE
             * Cuando es un producto compuestos, se usará la relación para definir
             * cuáles serán los productos que componen al producto principal
             *
             * En el caso de productos compuestos se define que:
             * En caso de que se elimine o actualice el producto raíz
             * Dichos ajustes también afectarán a los productos hijos (los que componen al producto raíz)
             */
            $table->unsignedInteger('product_id')->nullable()->default(null);
            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
