@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card mb-5">
                    <div class="card-header">
                        Listado de Productos

                        <div class="float-right">
                            <a href="#modal_upload_csv" data-toggle="modal" class="btn btn-sm btn-success">
                                Importar Comandos CSV
                            </a>
                        </div>
                    </div>

                    @if (session('status'))
                        <div class="card-body">
                            <div class="alert alert-success mb-0" role="alert">
                                {{ session('status') }}
                            </div>
                        </div>
                    @endif

                    @if (session('feedback_commands'))
                        <ul>
                            @foreach (session('feedback_commands') as $fb)
                                <li>
                                    <b>
                                        {{ $fb['product'] }}:
                                    </b>
                                    {{ $fb['action'] }}
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

            @forelse ($products as $product)

                <div @if ($product->isComposed()) class="col-md-6" @else class="col-md-3" @endif>

                    <div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title">
                                {{ $product->name }}

                                <span class="float-right">
                                    @if ($product->isComposed())
                                        <span class="badge badge-pill badge-secondary">C</span>
                                    @else
                                        <span class="badge badge-pill badge-primary">S</span>
                                    @endif
                                </span>
                            </h5>
                            <h6 class="card-subtitle mb-2 text-muted">
                                <b>
                                    Ref:
                                </b>
                                {{ $product->reference }}
                            </h6>

                            @if ($product->isComposed())
                                <p class="card-text">
                                    Producto Compuesto por:
                                </p>

                                <ul class="pl-4">
                                    @foreach ($product->products as $p)
                                        <li>
                                            {{ $product->name }}
                                            <small>
                                                <b>
                                                    Ref:
                                                </b>
                                                {{ $product->reference }}
                                            </small>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif

                            <table class="table mb-0">
                                <thead>
                                <tr>
                                    <td>
                                        Precio
                                    </td>
                                    <td class="number-view text-right">
                                        {{ $product->price }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Costo
                                    </td>
                                    <td class="number-view text-right">
                                        {{ $product->cost }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Unidades
                                    </td>
                                    <td class="number-view text-right">
                                        {{ $product->current_units }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Estado
                                    </td>
                                    <td class="text-right">
                                        @if ($product->state == 'active')
                                            <span class="badge badge-success">
                                            Activo
                                        </span>
                                        @elseif($product->state == 'inactive')
                                            <span class="badge badge-danger">
                                            Inactivo
                                        </span>
                                        @else
                                            <span class="badge badge-dark">
                                            Sin Estado
                                        </span>
                                        @endif
                                    </td>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                </div>

            @empty

                No existen productos

            @endforelse
        </div>

        {{ $products->links() }}
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="modal_upload_csv">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        Importar Comandos CSV
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Sistema para importar un archivo CSV con un listado de comandos
                    </p>

                    <div class="alert alert-dark" role="alert">
                        El archivo CSV debe estar separado por <code>,</code>.
                    </div>

                    <form action="{{ route('commands.csv') }}" enctype="multipart/form-data" method="POST">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label for="csv">
                                CSV
                            </label>
                            <span class="fit button-file">
                                <input type="file" class="form-control" name="csv" id="csv"
                                       data-validation="mime size required"
                                       data-validation-max-size="2M"
                                       data-validation-allowing="csv">
                                <span>Clic para abrir gestor de archivos</span>
                            </span>
                        </div>

                        <div class="text-center">
                            <button class="btn btn-success" id="btn_upload_csv">
                                Cargar Datos
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
