<?php

namespace Tests\Browser;

use App\Product;
use App\User;
use Illuminate\Support\Facades\Log;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;

class CommandsInProductTest extends DuskTestCase
{
    use DatabaseMigrations, ProductTrail;

    private $user = null;
    private $p1 = null;
    private $p2 = null;

    /**
     * A basic browser test example.
     *
     * @return void
     * @throws \Throwable
     */
    public function test_add_remove_active_inactive()
    {
        $this->runSeeder();
        $this->user = User::find(1);

        $this->initContext();

        $this->browse(function (Browser $browser) {
            $this->toContext($browser);
            $this->uploadCsv($browser);

            $browser->assertSee('El archivo ha sido cargado correctamente');

            $p1_text = $this->p1->name . ' (ref: ' . $this->p1->reference . ')';

                $browser->assertSee($p1_text)
                    ->assertSee( 'Se le agregó 2 unidades')
                    ->assertSee( 'Fue activado');

            $p2_text = $this->p2->name . ' (ref: ' . $this->p2->reference . ')';

            $browser->assertSee($p2_text)
                ->assertSee( 'Se le resto 4 unidades')
                ->assertSee( 'Fue desactivado');
        });
        /*
         * DATA INIT
         */
        /*
         * 'reference' => 1,
         * 'current_units' => 0,
         * 'state' => 'inactive'
         */
        $this->assertDatabaseHas('products', [
            'reference' => 1,
            'current_units' => 2,
            'state' => 'active'
        ]);

        /*
         * 'reference' => 2,
         * 'current_units' => 4,
         * 'state' => 'active'
         */
        $this->assertDatabaseHas('products', [
            'reference' => 2,
            'current_units' => 0,
            'state' => 'inactive'
        ]);

        $this->assertAndDeleteCsv();
    }
}
