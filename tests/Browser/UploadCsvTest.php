<?php

namespace Tests\Browser;

use App\Product;
use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;

class UploadCsvTest extends DuskTestCase
{
    use DatabaseMigrations, ProductTrail;

    private $user = null;

    /**
     * A basic browser test example.
     *
     * @return void
     * @throws \Throwable
     */
    public function test_upload_csv()
    {
        $this->runSeeder();
        $this->initContext();
        $this->user = User::find(1);

        $this->browse(function (Browser $browser) {
            $this->toContext($browser);

            $this->uploadCsv($browser);
        });

        $path_file = public_path() . '/storage/csv/command_current.csv';

        $this->assertTrue(file_exists($path_file));
        // Eliminamos el archivo para que las demás pruebas funcionen bien
        $this->assertTrue(unlink($path_file));
    }

    /**
     * A basic browser test example.
     *
     * @return void
     * @throws \Throwable
     */
    public function test_not_upload_csv_other_extension()
    {
        $this->runSeeder();

        $this->user = User::find(1);

        $this->browse(function (Browser $browser) {
            $this->toContext($browser);

            $browser->whenAvailable('#modal_upload_csv', function ($modal) {
                $modal->waitForText('Sistema para importar un archivo CSV con un listado de comandos')
                    ->attach('#csv', public_path() . '/storage/example.png')
                    ->press('#btn_upload_csv')
                    ->assertSee('Sólo los archivos del tipo csv están permitido');
            });
        });

        $path_file = public_path() . '/storage/csv/command_current.csv';

        $this->assertFalse(file_exists($path_file));
    }
}
