<?php

namespace Tests\Browser;

use App\Product;
use Illuminate\Support\Facades\Artisan;

trait ProductTrail
{
    public function runSeeder() {
        Artisan::call('db:seed');
    }

    public function initContext()
    {
        /* Seeder para Unit Test */
        $this->p1 = factory(Product::class)->create([
            'product_id' => null,
            'reference' => 1,
            'current_units' => 0,
            'state' => 'inactive'
        ]);

        $this->p2 = factory(Product::class)->create([
            'product_id' => null,
            'reference' => 2,
            'current_units' => 4,
            'state' => 'active'
        ]);
    }

    public function toContext($browser)
    {
        $browser->loginAs($this->user)
            ->visitRoute('home')
            ->waitForText('Listado de Productos')
            ->clickLink('Importar Comandos CSV');
    }


    public function uploadCsv($browser) {
        $browser->whenAvailable('#modal_upload_csv', function ($modal) {
            $modal->waitForText('Sistema para importar un archivo CSV con un listado de comandos')
                ->attach('#csv', public_path() . '/storage/unit_test.csv')
                ->press('#btn_upload_csv');
        });
    }

    public function assertAndDeleteCsv() {
        $path_file = public_path() . '/storage/csv/command_current.csv';

        $this->assertTrue(file_exists($path_file));
        // Eliminamos el archivo para que las demás pruebas funcionen bien
        $this->assertTrue(unlink($path_file));
    }
}